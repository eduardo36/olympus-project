<?php
//Activamos el almacenamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("Location: login.html");
}
else
{
require 'header.php';

if ($_SESSION['almacen']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                    <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
                              <thead style="background-color:#A9D0F5">
                                    <th>configuracion</th>
                                    <th>Numero de orden</th>
                                    <th>Fecha Estimada</th>
                                    <th>Responsable</th>
                                    <th>Cliente</th>
                                    <th>Custodia #</th>
                                    <th>Actividad</th>
                                    <th>Avance %</th>
                                    <th>Imagen</th>
                                </thead>
                                <tfoot style="background-color:#A9D0F5">
                                    <th>configuracion</th>
                                    <th>Numero de orden</th>
                                    <th>Fecha Estimada</th>
                                    <th>Responsable</th>
                                    <th>Cliente</th>
                                    <th>Custodia #</th>
                                    <th>Actividad</th>
                                    <th>Avance %</th>
                                    <th>Imagen</th>
                                </tfoot>
                                <tbody>
                                  
                                </tbody>
                            </table>
                    </div>
                    <div class="panel-body" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Numero de Orden(*):</label>
                            <input type="hidden" name="idorden" id="idorden">
                            <input type="text" class="form-control" name="numero_orden" id="numero_orden" maxlength="100" placeholder="Numero de orden" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Fecha estimada(*):</label>
                            <input type="date" class="form-control" name="fecha_hora" id="fecha_hora" required="">
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Responsable(*):</label>
                            <select id="responsable" name="responsable" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Anexar Responsable:</label>
                            <select id="anexo_responsable" name="anexo_responsable" class="form-control selectpicker" data-live-search="true" required aria-required="true">
                            
                            </select>
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Cliente(*):</label>
                            <select id="cliente" name="cliente" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Cadena de custodia(*):</label>
                            <input type="number" class="form-control" name="cad_custodia" id="cad_custodia" required>
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Acción:</label>
                            <input type="text" class="form-control" name="descripcion" id="descripcion" maxlength="2000" placeholder="Descripción">
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Avance (%):</label>
                            <meter id="avance" style="width:100%;" min="0" max="100" low="25" high="75" optimum="100" value="">
                           </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Avance (%):</label>
                            <select id="avance_" name="avance_" class="form-control selectpicker" data-live-search="true" required>
                              <option value='25'> 25% </option>
                              <option value='50'> 50% </option>
                              <option value='75'> 75% </option>
                              <option value='100'> 100% </option>
                            </select>
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Imagen:</label>
                            <input type="file" class="form-control" name="imagen" id="imagen">
                            <input type="hidden" name="imagenactual" id="imagenactual">
                            <img src="" width="150px" height="120px" id="imagenmuestra">
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require 'noacceso.php';
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/categoria.js"></script>
<?php 
}
ob_end_flush();
?>


