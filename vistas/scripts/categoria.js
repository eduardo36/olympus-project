var tabla;

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	});

	$.post("../ajax/articulo.php?op=selectCategoria", function(r){
		$("#responsable").append('<option value=""> -- seleccione una opcion -- </option>');		
		$("#responsable").append(r);
		$('#responsable').selectpicker('refresh');
	});

	$.post("../ajax/articulo.php?op=selectGeneral", function(r){
		$("#anexo_responsable").append("<option disabled selected> -- seleccione una opción -- </option>");
		$("#anexo_responsable").append(r);
		$('#anexo_responsable').selectpicker('refresh');
	})
	//Cargamos los items de resposable
	$.post("../ajax/articulo.php?op=selectCliente", function(r){
		$('#cliente').append('<option value=""> -- Seleccione una opcion --</option>');	
		$("#cliente").append(r);
		$('#cliente').selectpicker('refresh');
	});

    $('#mAlmacen').addClass("treeview active");
    $('#lCategorias').addClass("active");
}

//Función limpiar
function limpiar()
{
	$("#idcategoria").val("");
	$("#nombre").val("");
	$("#descripcion").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#detalles').dataTable(
	{
		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/articulo.php?op=listarFull',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/categoria.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idarticulo)
{
	$.post("../ajax/escritorio.php?op=mostrar",{idarticulo : idarticulo}, function(data, status)
	{
		data = JSON.parse(data);	
	
		mostrarform(true);

		$("#idorden").val(data.idorden);
		$("#numero_orden").val(data.num_orden);
		$("#numero_orden").prop("disabled",true);
		$("#fecha_hora").val(data.fecha_estimada);
		$("#responsable").val(data.responsable);
		$("#responsable").selectpicker('refresh');
		$("#cliente").selectpicker('refresh');
		$("#cliente").val(data.cliente);
		$("#avance").val(data.avance);
		$("#cad_custodia").val(data.cad_custodia);
		$("#descripcion").val(data.descripcion);
		$("#imagenmuestra").show();
		$("#imagenmuestra").attr("src","../files/articulos/"+data.imagen);
		$("#imagenactual").val(data.imagen);


 	})
}

//Función para desactivar registros
function desactivar(idarticulo)
{
	bootbox.confirm("¿Está Seguro que desea reactivar la orden?", function(result){
		if(result)
        {
        	$.post("../ajax/articulo.php?op=activar", {idarticulo : idarticulo}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idorden)
{
	bootbox.confirm("¿Está Seguro que desea reactivar la Orden? \n \r Esto podra causar envios de correos de reactivación", function(result){
		if(result)
        {
        	$.post("../ajax/articulo.php?op=activar", {idorden : idorden}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

function actividad(idarticulo) {
	$.post("../ajax/escritorio.php?op=actividad",{idarticulo:idarticulo},function(e){
		e = JSON.parse(e);
		console.log(e);
		
		bootbox.alert({
			message: e.descripcion,
			size: 'large'
		});
	});
	
}
init();