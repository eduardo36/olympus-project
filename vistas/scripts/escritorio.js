var tabla;
var cadena_cust,avance;

//Función que se ejecuta al inicio
function init(){
	mostrarform(true);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

//Cargamos los items de responsble
	$.post("../ajax/articulo.php?op=selectCategoria", function(r){
		$("#responsable").append('<option value=""> -- seleccione una opcion -- </option>');		
		$("#responsable").append(r);
		$('#responsable').selectpicker('refresh');
	});

	$.post("../ajax/articulo.php?op=selectGeneral", function(r){
		$("#anexo_responsable").append("<option disabled selected> -- seleccione una opción -- </option>");
		$("#anexo_responsable").append(r);
		$('#anexo_responsable').selectpicker('refresh');
	})
	//Cargamos los items de resposable
	$.post("../ajax/articulo.php?op=selectCliente", function(r){
		$('#cliente').append('<option value=""> -- Seleccione una opcion --</option>');	
		$("#cliente").append(r);
		$('#cliente').selectpicker('refresh');
	});

	$("#imagenmuestra").hide();
	$('#mAlmacen').addClass("treeview active");
	$('#lArticulos').addClass("active");
	
	$("#fecha_hora").change(function(){
		actualizarOrden(0,$("#idorden").val(),$("#fecha_hora").val());
	});

	setTimeout(refrescar,300000);
}
function refrescar(){
	if ($("#formularioregistros").is(':visible')){

	}else{
		location.reload();
	}
	
}
// ENUMERA CADA UNA DE LOS INPUTS DEL FORM EN EL ARRAY DE LA FUNCION actualizacionOrden()
// y en la funcion de modificar oren en el modelos de Articulo
// fecha_hora = 0
// anexo_responsable = 1
// cad_custodia = 2
// avance_ = 3
//////////////////////////////////////////////

function actualizarOrden(cambio,idorden,load){
var cambiosOrden = ['fecha_hora','anexo_responsable','cad_custodia','avance_'];
		bootbox.prompt({
			title: "Estas a un paso de modificar la orden. Este cambio sera notiicado a todos los suscritos a esta orden <h3> Observaciones: </h3>",
			centerVertical: true,
			buttons: {
				confirm: {
					label: 'Hacer cambios',
					className: 'btn-success'
				},
				cancel: {
					label: 'Cancelar',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result != null){
					console.log("estoy en el callback true y con los siguente datos " + cambiosOrden[cambio]+' '+idorden);
					$.post("../ajax/articulo.php?op=change",{
						flag:cambiosOrden[cambio],
						idorden:idorden,
						payload:load,
						comentario:result
					},
						function(r){
							bootbox.alert(r);
							mostrarform(true);
							tabla.ajax.reload();
					});
				}else{
				}
			}
		});
	}
//Función limpiar
function limpiar()
{
	$("#codigo").val("");
	$("#nombre").val("");
	$("#descripcion").val("");
	$("#stock").val("");
	$("#imagenmuestra").attr("src","");
	$("#imagenactual").val("");


}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
	}
	else
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(true);
}

//Función Listar
function listar()
{
	tabla=$('#detalles').dataTable(
	{
		"lengthMenu": [ 5, 10, 25, 75, 100],//mostramos el menú de registros a revisar
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/articulo.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"language": {
            "lengthMenu": "Mostrar : _MENU_ registros",
            "buttons": {
            "copyTitle": "Tabla Copiada",
            "copySuccess": {
                    _: '%d líneas copiadas',
                    1: '1 línea copiada'
                }
            }
        },
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disable",true);
	
	
	/*$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/articulo.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }
	}); */
	limpiar();
}

function mostrar(idarticulo)
{
	$.post("../ajax/escritorio.php?op=mostrar",{idarticulo : idarticulo}, function(data, status)
	{
		data = JSON.parse(data);	
	
		mostrarform(false);

		$("#idorden").val(data.idorden);
		
		$("#numero_orden").val(data.num_orden);
		$("#numero_orden").prop("disabled",true);
		$("#fecha_hora").val(data.fecha_estimada);
		$("#responsable").val(data.responsable);
		$("#responsable").selectpicker('refresh');
		$("#cliente").selectpicker('refresh');
		$("#cliente").val(data.cliente);
		$("#cliente").prop("disabled",true);
		$("#avance").val(data.avance);
		$("#cad_custodia").val(data.cad_custodia);
		$("#descripcion").val(data.descripcion);
		$("#imagenmuestra").show();
		$("#imagenmuestra").attr("src","../files/articulos/"+data.imagen);
		$("#imagenactual").val(data.imagen);
 	})
}

//Función para desactivar registros
function desactivar(idarticulo)
{
	bootbox.confirm("¿Está Seguro terminar la orden?", function(result){
		if(result)
        {
        	$.post("../ajax/articulo.php?op=desactivar", {idarticulo : idarticulo}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idorden)
{
	bootbox.confirm("¿Está Seguro de terminar la Orden?", function(result){
		if(result)
        {
        	$.post("../ajax/articulo.php?op=desactivar", {idorden : idorden}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

function actividad(idarticulo) {
	$.post("../ajax/escritorio.php?op=actividad",{idarticulo:idarticulo},function(e){
		e = JSON.parse(e);
		console.log(e);
		
		bootbox.alert({
			message: e.descripcion,
			size: 'large'
		});
	});
	
}
init();