<?php
//Activamos el almacenamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("Location: login.html");
}
else
{
require 'header.php';

if ($_SESSION['cliente_ext']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Informes </h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body" id="listadoregistros">
                      <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
                              <thead style="background-color:#d1a4a5">
                                    <th>Empresa</th>
                                    <th>Folio Muestra</th>
                                    <th>Folio Cliente</th>
                                    <th>Tipo Analisis</th>
                                    <th>Descarga</th>
                             
                             
                                </thead>
                                <tfoot style="background-color:#d1a4a5">
                                    <th>Empresa</th>
                                    <th>Folio Muestra</th>
                                    <th>Folio Cliente</th>
                                    <th>Tipo Analisis</th>
                                    <th>Descarga</th>
                                </tfoot>
                                <tbody>
                                  
                                </tbody>
                            </table>
                          </div>
                    </div>

                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require 'noacceso.php';
}

require 'footer.php';
?>

<script src="../public/js/chart.min.js"></script>
<script src="../public/js/Chart.bundle.min.js"></script> 
<script src="scripts/clienteExt.js"></script>
<?php 
}
ob_end_flush();
?>


