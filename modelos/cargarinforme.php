<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Informe
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idpersona,$folio_muestra,$folio_cliente,$tipo_analisis,$url_archivo,$condicion)
	{
		$sql="INSERT INTO informes (idpersona,folio_muestra,folio_cliente,tipo_analisis,url_archivo,condicion)
		VALUES ('$idpersona','$folio_muestra','$folio_cliente','$tipo_analisis','$url_archivo','$condicion')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idorden,$numero_orden,$fecha_hora,$responsable,$anexo_responsable,$cliente,$cad_custodia,$descripcion,$avance,$imagen)
	{
		$sql="UPDATE orden SET num_orden='$numero_orden',fecha_estimada='$fecha_hora',responsable='$responsable',anexo_responsable='$anexo_responsable',cliente='$cliente',cad_custodia='$cad_custodia',descripcion='$descripcion',avance='$avance' WHERE idorden='$idorden'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar registros
	public function desactivar($idorden)
	{
		$sql="UPDATE orden SET act_des='0' WHERE idorden='$idorden'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idorden)
	{
		$sql="UPDATE orden SET act_des='1' WHERE idorden='$idorden'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idorden)
	{
		$sql="SELECT * FROM orden WHERE idorden='$idorden'";
		return ejecutarConsultaSimpleFila($sql);
	}
		//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrarEmail($idorden)
	{
		$sql="SELECT b.email as generadorOrden, c.email as responsable, d.email as auxiliar FROM orden a INNER JOIN usuario b ON b.idusuario=a.generador INNER JOIN usuario c ON a.responsable=c.idusuario INNER JOIN usuario d ON a.anexo_responsable=d.idusuario WHERE a.idorden='$idorden'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT a.idinforme,b.nombre as cliente,a.folio_muestra,a.folio_cliente,a.tipo_analisis,a.url_archivo FROM informes a INNER JOIN persona b ON a.idpersona=b.idpersona";
		return ejecutarConsulta($sql);		
	}

	public function listarFull()
	{
		$sql="SELECT a.idorden,a.num_orden,a.fecha_estimada,c.nombre as responsable,d.nombre AS cliente,a.cad_custodia,a.descripcion,a.avance,a.imagen FROM orden a INNER JOIN usuario c ON a.responsable=c.idusuario INNER JOIN persona d ON a.cliente=d.idpersona ORDER BY `a`.`idorden` ASC";
		return ejecutarConsulta($sql);		
	}

	//Implementar un método para listar los registros activos
	public function listarActivos()
	{
		$sql="SELECT a.idarticulo,a.idcategoria,c.nombre as categoria,a.codigo,a.nombre,a.stock,a.descripcion,a.imagen,a.condicion FROM articulo a INNER JOIN categoria c ON a.idcategoria=c.idcategoria WHERE a.condicion='1'";
		return ejecutarConsulta($sql);		
	}

	//Implementar un método para listar los registros activos, su último precio y el stock (vamos a unir con el último registro de la tabla detalle_ingreso)
	public function listarActivosVenta()
	{
		$sql="SELECT a.idarticulo,a.idcategoria,c.nombre as categoria,a.codigo,a.nombre,a.stock,(SELECT precio_venta FROM detalle_ingreso WHERE idarticulo=a.idarticulo order by iddetalle_ingreso desc limit 0,1) as precio_venta,a.descripcion,a.imagen,a.condicion FROM articulo a INNER JOIN categoria c ON a.idcategoria=c.idcategoria WHERE a.condicion='1'";
		return ejecutarConsulta($sql);		
	}

	public function actividad($idorden){
		$sql="SELECT descripcion FROM orden WHERE idorden='$idorden'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function modifcarOrden($flag,$idorden,$load,$generador,$comentario){
		$modificacion = [
			'fecha_hora' => 'fecha_estimada',
			'anexo_responsable'=>'anexo_responsable',
			'cad_custodia'=>'cad_custodia',
			'avance_'=>'avance'
		];

		$sql="UPDATE orden SET $modificacion[$flag]='$load' WHERE idorden='$idorden'";
		ejecutarConsulta($sql);

		$sql = "INSERT INTO bitacora (idorden,generador,cambio,actualizacion,comentarios) VALUES('$idorden','$generador','$modificacion[$flag]','$load','$comentario')";
		return ejecutarConsulta($sql); 
	}
}

?>