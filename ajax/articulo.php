<?php 
ob_start();
if (strlen(session_id()) < 1){
	session_start();//Validamos si existe o no la sesión
}
if (!isset($_SESSION["nombre"]))
{
  header("Location: ../vistas/login.html");//Validamos el acceso solo a los usuarios logueados al sistema.
}
else
{
//Validamos el acceso solo al usuario logueado y autorizado.
if ($_SESSION['almacen']==1)
{	
require_once "../modelos/Articulo.php";
require_once "../modelos/mydateTime.php";



$articulo=new Articulo();
$generador = $_SESSION["idusuario"];

$flag=isset($_POST["flag"])? limpiarCadena($_POST["flag"]):""; 
$load=isset($_POST["payload"])? limpiarCadena($_POST["payload"]):""; 
$idorden=isset($_POST["idorden"])? limpiarCadena($_POST["idorden"]):"";
$comentario=isset($_POST["comentario"])? limpiarCadena($_POST["comentario"]):"";


$numero_orden=isset($_POST["numero_orden"])? limpiarCadena($_POST["numero_orden"]):"";
$fecha_hora=isset($_POST["fecha_hora"])? limpiarCadena($_POST["fecha_hora"]):"";
$responsable=isset($_POST["responsable"])? limpiarCadena($_POST["responsable"]):"";
$anexo_responsable=isset($_POST["anexo_responsable"])? limpiarCadena($_POST["anexo_responsable"]):"";
$cliente=isset($_POST["cliente"])? limpiarCadena($_POST["cliente"]):"";
$cad_custodia=isset($_POST["cad_custodia"])? limpiarCadena($_POST["cad_custodia"]):"";
$avance=isset($_POST["avance_"])? limpiarCadena($_POST["avance_"]):"";
$descripcion=isset($_POST["descripcion"])? limpiarCadena($_POST["descripcion"]):"";
$imagen=isset($_POST["imagen"])? limpiarCadena($_POST["imagen"]):"";

$fecha_actual =  new DateTime("now", new DateTimeZone('America/Mexico_City'));


switch ($_GET["op"]){
	case 'guardaryeditar':

		if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name']))
		{
			$imagen=$_POST["imagenactual"];
		}
		else 
		{
			$ext = explode(".", $_FILES["imagen"]["name"]);
			if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png")
			{
				$imagen = round(microtime(true)) . '.' . end($ext);
				move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/articulos/" . $imagen);
			}
		}
		if (empty($idorden)){
			$rspta=$articulo->insertar($idorden,$generador,$numero_orden,$fecha_hora,$responsable,$anexo_responsable,$cliente,$cad_custodia,$descripcion,$imagen,$fecha_actual);
			if ($rspta){
				echo "Orden registrada";

				require("../sendgrid-php/sendgrid-php.php");
				
				require_once "../modelos/Usuario.php";
				$usuarioN =new Usuario();

				//primer responsable
				$emailResponsable0=$usuarioN->listarUser($responsable);
				$emailResponsable0 = $emailResponsable0['email'];
				
				//generador de orden
				$emailResponsable2=$usuarioN->listarUser($generador);
				$emailResponsable2 = $emailResponsable2['email'];
				
				$email = new \SendGrid\Mail\Mail(); 
				$email->setFrom("silminformativo@silm.com.mx", "Sistema Olympus");

				if(empty($anexo_responsable)){
					
					$tos = ["eduardo@yetox.com"   => "default", 
							"jorge.tun@silm.com.mx" => "default",
							"miguel.narvaez@silm.com.mx" => "default",
							 "$emailResponsable0" => "PrimerResponsable",
							 "$emailResponsable2" => "GeneradorOrden"
							];
				}else {
					//segundo responsble
					$emailResponsable1=$usuarioN->listarUser($anexo_responsable);
					$emailResponsable1 = $emailResponsable1['email'];
					$tos = [
						"eduardo@yetox.com" => "Default",
						"jorge.tun@silm.com.mx" => "default",
						"miguel.narvaez@silm.com.mx" => "default",
						"$emailResponsable0" => "PrimerResponsable",
						"$emailResponsable1" => "AnexoResponsable",
						"$emailResponsable2" => "GeneradorOrden"
					]; 
				}

				$email->addTos($tos);
				$email->setSubject("Hola, tienes una nueva order asignada numero: ".$numero_orden);
				$email->addContent("text/plain", "and easy to do anywhere, even with PHP");
				$email->addContent(
					"text/html", "<strong>Puedes revisar en tu tabla dinamica la orden a la cual fuste asignado</strong>"
				);
				$sendgrid = new \SendGrid('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');
				try {
					$response = $sendgrid->send($email);
					//print $response->statusCode() . "\n";
					//print_r($response->headers());
					//print $response->body() . "\n";
				} catch (Exception $e) {
					echo 'Caught exception: '. $e->getMessage() ."\n";
				}

			
			}else{

				}
			
			//echo $rspta ? "Orden registrada" : "No se pudo registrar orden";
		}
		else {
			$rspta=$articulo->editar($idorden,$numero_orden,$fecha_hora,$responsable,$anexo_responsable,$cliente,$cad_custodia,$descripcion,$avance,$imagen);
			echo $rspta ? "Artículo actualizado" : "Artículo no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$articulo->desactivar($idorden);
 		echo $rspta ? "Orden Terminada" : "La Orden no puede terminar";
	break;

	case 'activar':
		$rspta=$articulo->activar($idorden);
		 echo $rspta ? "Orden Activada" : "Orden no se puede activar";
		 

		 
	break;

	case 'mostrar':
		$rspta=$articulo->mostrar($idorden);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
	break;

	case 'listar':
		$rspta=$articulo->listar();
 		//Vamos a declarar un array
 		$data= array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
				 "0"=>'<button class="btn btn-warning" onclick="mostrar('.$reg->idorden.')"><i class="fa fa-pencil"></i></button>'.
				 '<button class="btn btn-primary" onclick="activar('.$reg->idorden.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->num_orden,
 				"2"=>$reg->fecha_estimada,
 				"3"=>$reg->responsable,
				"4"=>$reg->cliente,
				"5"=>$reg->cad_custodia,
				"6"=>'<button class="btn btn-success" onclick="actividad('.$reg->idorden.')"><i class="fa fa-plus-circle"></i> Mostrar </button>', 
				"7"=>($reg->avance) ? '<meter style="width:100%;" min="0" max="100" low="25" high="75" optimum="100" value="'.$reg->avance.'">' :
				'<meter style="width:100%;" min="0" max="100" low="25" high="75" optimum="100" value="'.$reg->avance.'">' ,
 				"8"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px' >",
				"9"=>'<span class="label bg-green">Activado</span>'.
				 '<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case 'listarFull':
		$rspta=$articulo->listarFull();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
				 "0"=>'<button class="btn btn-warning" onclick="mostrar('.$reg->idorden.')"><i class="fa fa-pencil"></i></button>'.
				 '<button class="btn btn-primary" onclick="activar('.$reg->idorden.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->num_orden,
 				"2"=>$reg->fecha_estimada,
 				"3"=>$reg->responsable,
				"4"=>$reg->cliente,
				"5"=>$reg->cad_custodia,
				"6"=>'<button class="btn btn-success" onclick="actividad('.$reg->idorden.')"><i class="fa fa-plus-circle"></i> Mostrar </button>', 
				"7"=>($reg->avance) ? '<meter style="width:100%;" min="0" max="100" low="25" high="75" optimum="100" value="'.$reg->avance.'">' :
				'<meter style="width:100%;" min="0" max="100" low="25" high="75" optimum="100" value="'.$reg->avance.'">' ,
 				"8"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px' >",
				"9"=>'<span class="label bg-green">Activado</span>'.
				 '<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "selectCategoria":
		require_once "../modelos/Categoria.php";
		$categoria = new Categoria();

		$rspta = $categoria->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idusuario . '>' . $reg->nombre . ' '.$reg->tipo_documento. '</option>';
				}
	break;

	case "selectGeneral":
		require_once "../modelos/Categoria.php";
		$categoria = new Categoria();

		$rspta = $categoria->selectGeneral();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idusuario . '>' . $reg->nombre . ' '.$reg->tipo_documento. '</option>';
				}
	break;

	case "selectCliente":
		require_once "../modelos/Categoria.php";
		$categoria = new Categoria();

		$rspta = $categoria->selectCliente();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpersona . '>' .'Proyecto: '.$reg->direccion.' '.$reg->nombre.'  '.'</option>';
				}
	break;
	////////////////////////////////////////////////////////
	//// configuracion para actualizaciones individuales
	case "change":
	$rspta = $articulo->modifcarOrden($flag,$idorden,$load,$generador,$comentario);
	//echo $rspta? 'Modifcación exitosa':'No se pudo realizar la actualizacion';
	
	$rspta = $articulo->mostrarEmail($idorden);
	
	$rspta = array_flip($rspta);

	print_r($rspta);

	require("../sendgrid-php/sendgrid-php.php");
	$email = new \SendGrid\Mail\Mail(); 
	$email->setFrom("silminformativo@silm.com.mx", "Sistema Olympus");

	$email->addTos($rspta);
	$email->setSubject("Hola, se realizaron cambion en la orden numero: ".$numero_orden);
	$email->addContent("text/plain", "and easy to do anywhere, even with PHP");
	$email->addContent(
		"text/html", "<strong>Se realizaron cambios en la orden numero: ".$idorden."</strong>
		<span>Comentario: ".$comentario."</span>"
		
	);
	$sendgrid = new \SendGrid('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');
	try {
		$response = $sendgrid->send($email);
		//print $response->statusCode() . "\n";
		//print_r($response->headers());
		//print $response->body() . "\n";
	} catch (Exception $e) {
		echo 'Caught exception: '. $e->getMessage() ."\n";
	}


	break;
}
//Fin de las validaciones de acceso
}
else
{
  require 'noacceso.php';
}
}
ob_end_flush();
?>